//1
let array = [1, 2, 7, 8, 9, 10, 3, 4, 5, 6, 11, 17, 18, 19, 20, 12, 13, 14, 15, 16]
function bubbleSort(arr) {
    let len = array.length
    for (let i = 0, endI = len - 1; i < endI; i++) {
        let wasSwap = false;
        for (let j = 0, endJ = endI - i; j < endJ; j++) {
            if (array[j] > array[j + 1]) {
                let swap = array[j];
                array[j] = array[j + 1];
                array[j + 1] = swap;
                wasSwap = true;
            }
        }
        if (!wasSwap) break;
    }
    return arr;
}
console.log(bubbleSort(array))

//2
let salaries  = [
    {name:'ivan', salary:300},
    {name:'vova', salary:500},
    {name:'zigmund', salary:1300}
    ]

function sumSalaries(salaries) {
    let len = salaries.length
    if (len === 0) {
        return 0
    }
    let summa = 0
    for (let i = 0; i < len; i++) {
        summa += salaries[i].salary
    }
    return summa
}
console.log(sumSalaries(salaries))

//3
let salaries  = [
    {name:'ivan', salary:300},
    {name:'vova', salary:500},
    {name:'zigmund', salary:1300}
]
function groupSalaries(arr) {
    let len = salaries.length
    let objSalaries =
        {
            'high': [],
            'middle': [],
            'low': []
        }
    for (let i = 0; i < len; i++) {
        console.log(arr[i])
        objSalaries[i] = arr[i].salary
        if (arr[i].salary >= 1000 && arr[i].salary <= 2000) {
            objSalaries.high[i] = arr[i].salary
        }
        if (arr[i].salary >= 300 && arr[i].salary <= 600) {
            objSalaries.middle[i] = arr[i].salary
        }
        if (arr[i].salary < 300) {
            objSalaries.low[i] = arr[i].salary
        }
    }
    return objSalaries
}
console.log(groupSalaries(salaries))







//1
let array = ['AlEX', 'Vanya', 'Tanya', 'Lena', 'Tolya']
function firstLetterLower(arr) {
    let len = arr.length
    for (let i = 0; i < len; i++) {
        arr[i] = arr[i].toLowerCase()
    }
    return arr
}
console.log(firstLetterLower(array))

//2
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let len = array.length
function sort(arr) {
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
        }
    }
    return arr;
}
console.log(sort(array))

//3
class User {
    constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty) {
        this.firstName = firstName
        this.secondName = secondName
        this.lastName = lastName
        this.age = age
        this.receiptDate = receiptDate
        this.dateOfIssue = dateOfIssue
        this.specialty = specialty
    }

    shortName() {
        return this.secondName[0].toUpperCase() + this.secondName.substr(1) + ' ' + this.firstName[0].toUpperCase() + '. ' + this.lastName[0].toUpperCase() + '.'
    }
}

let alina = new User('alina', 'fedyniak', 'vitalievna', 28, 2010, 2015, 'designer')
console.log(alina.shortName())

class Student extends User {
    constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty, grade) {
        super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty)
        this.grade = grade
    }

    ageStudent() {
        return this.age
    }

    getDiplome() {
        if (this.grade < 60) {
            return 'диплом не получен'
        }
        if (this.grade > 60) {
            return 'диплом получен'
        }
    }

    studyDurationYear() {
        return this.dateOfIssue - this.receiptDate + ' лет'
    }

    studyDurationMonth() {
        return (this.dateOfIssue - this.receiptDate) * 12 + ' месяцев'
    }

}

let student1 = new Student('alina', 'fedyniak', 'vitalievna', 28, 2010, 2015, 'designer', 95)
console.log(student1.ageStudent())
console.log(student1.getDiplome())
console.log(student1.studyDurationYear())
console.log(student1.studyDurationMonth())

class Group extends User {
    constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty, issueOrDeduction) {
        super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty)
        this.issueOrDeduction = issueOrDeduction

    }
}


//1
//for
let array = [12, 22, -1245, -1, 0, 4994, 555]
let min = 0
let max = 0
let arrayLength = array.length
for (let i = 0; i < arrayLength; i++) {
    if (max < array[i]) {
        max = array[i]
    }
    if (min > array[i]) {
        min = array[i]
    }
}
let middleNumber = (min + max)/2
console.log(max)
console.log(min)
console.log(middleNumber)

//while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let min = 0
let max = 0
let i = 0
let arrayLength = array.length
while (i < arrayLength) {
    if (max < array[i]) {
        max = array[i]
    }
    if (min > array[i]) {
        min = array[i]
    }
    i++
}
let middleNumber = (min + max)/2
console.log(max)
console.log(min)
console.log(middleNumber)

//do while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let min = 0
let max = 0
let i = 0
let arrayLength = array.length
do {
    let array = [12, 22, -1245, -1, 0, 4994, 555]
    let min = 0
    let max = 0
    let i = 0
    let arrayLength = array.length
    while (i < arrayLength) {
        if (max < array[i]) {
            max = array[i]
        }
        if (min > array[i]) {
            min = array[i]
        }
        i++
    }
    let middleNumber = (min + max)/2
    console.log(max)
    console.log(min)
    console.log(middleNumber)
    i++
}while (i < arrayLength)
let middleNumber = (min + max)/2
console.log(max)
console.log(min)
console.log(middleNumber)

//for of
let array = [12, 22, -1245, -1, 0, 4994, 555]
let min = 0
let max = 0
let i = 0
for (let value of array) {
    if (max < array[i]) {
        max = array[i]
    }
    if (min > array[i]) {
        min = array[i]
    }
    i++
}
let middleNumber = (min + max)/2
console.log(max)
console.log(min)
console.log(middleNumber)

//for in
let array = [12, 22, -1245, -1, 0, 4994, 555]
let min = 0
let max = 0
let i = 0
for (let value in array) {
    if (max < array[i]) {
        max = array[i]
    }
    if (min > array[i]) {
        min = array[i]
    }
    i++
}
let middleNumber = (min + max)/2
console.log(max)
console.log(min)
console.log(middleNumber)

//2
//for of
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = 0
for (let value of array) {
    arrayLength++

}
console.log(arrayLength)

//for in
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = 0
for (let value in array) {
    arrayLength++

}
console.log(arrayLength)

//for
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = 0
for (let i = 0; i <= arrayLength; i++) {
    if (array[i] === undefined) {
        break
    }
    arrayLength++
}
console.log(arrayLength)

//while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = 0
let i = 0
while (i <= arrayLength) {
    console.log(array[i])
    if (array[i] === undefined) {
        break
    }
    i++
    arrayLength++
}
console.log(arrayLength)

//do while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = 0
let i = 0
do {
    if (array[i] === undefined) {
        break
    }
    i++
    arrayLength++
} while (i <= arrayLength)
console.log(arrayLength)

//3
//for
let array = [12, 22, -1245, -1, 0, 4994, 555]
let arrayLength = array.length
for (let i = 0; i < arrayLength; i++) {
    if (array[i] != 0) {
        array[i] = array[i] * -1;
    }
}
console.log(array)

//while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0
let arrayLength = array.length
while (i < arrayLength) {
    if (array[i] != 0) {
        array[i] = array[i] * -1;
    }
    i++
}
console.log(array)

//do while
let array = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0
let arrayLength = array.length
do {
    if (array[i] != 0) {
        array[i] = array[i] * -1;
    }
    i++
}while (i < arrayLength)

console.log(array)

//for of
let array = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0
for (let value of array) {
    if (array[i] != 0) {
        array[i] = array[i] * -1;
    }
    i++
}
console.log(array)

let array = [12, 22, -1245, -1, 0, 4994, 555]
let i = 0
for (let value in array) {
    if (array[i] != 0) {
        array[i] = array[i] * -1;
    }
    i++
}
console.log(array)

//4
//for
let array = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let arrayPositive = []
let count = 0;
let arrayLength = array.length
for (let i = 0; i < arrayLength; i++) {
    if (array[i] > 0 && array[i] < 12) {
        arrayPositive[count] =  array[i]
        count++
    }
}console.log(arrayPositive)

//while
let array = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let arrayPositive = []
let count = 0;
let i = 0
let arrayLength = array.length
while (i < arrayLength) {
    if (array[i] > 0 && array[i] < 12) {
        arrayPositive[count] =  array[i]
        count++
    }
    i++
}
console.log(arrayPositive)

//do while
let array = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let arrayPositive = []
let count = 0;
let i = 0
let arrayLength = array.length
do {
    if (array[i] > 0 && array[i] < 12) {
        arrayPositive[count] =  array[i]
        count++
    }
    i++
} while (i < arrayLength)
console.log(arrayPositive)

//for of
let array = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let arrayPositive = []
let count = 0;
let i = 0
for (let value of array) {
    if (array[i] > 0 && array[i] < 12) {
        arrayPositive[count] =  array[i]
        count++
    }
    i++
}
console.log(arrayPositive)

//for in
let array = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
let arrayPositive = []
let count = 0;
let i = 0
for (let value of array) {
    if (array[i] > 0 && array[i] < 12) {
        arrayPositive[count] =  array[i]
        count++
    }
    i++
}
console.log(arrayPositive)

//5
let array =[
    [
    car = "Nissan",
    model = "Leaf",
    color = "green",
    year = 2015,
    engine = 1.6
    ],
    [
     car = "bmw",
     model = "i3",
     color = "black",
     year = 2018,
     engine = 2.0
    ],
    [
     car = "bmw",
     model = "x5",
     color = "orange",
     year = 2016,
     engine = 3.0
    ],
    [
     car = "mersedes",
     model = "e220",
     color = "blue",
     year = 2014,
     engine = 2.0
    ],
    [
    car = "волга",
    model = "2410",
    color = "black",
    year = 1989,
    engine = 2.4
    ],
]
let arrayLength = array.length
for (let i = 0, endI = arrayLength - 1; i < endI; i++) {
    let wasSwap = false;
    for (let j = 0, endJ = endI - i; j < endJ; j++) {
        if (array[j][3] > array[j + 1][3]) {
            [array[j][3], array[j + 1][3]] = [array[j + 1][3], array[j][3]];
            wasSwap = true;
        }
    }
    if (!wasSwap) break;
}
console.log(array)


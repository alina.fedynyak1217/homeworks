//1 найти площадь
//Declaration
function s(a, b) {
    return  a * b
}
console.log(s(7,5))

//Expression
let s = function(a, b) {
    return  a * b
}
console.log(s(7,5))

//стрелочная
let s = (a, b) => a * b
console.log(s(7,5))

//2 пифагора
//Declaration
function pythagorean(a, b) {
    return ((a ** 2) + (b ** 2)) ** (1/2);
}
console.log(pythagorean(6, 8))

//Expression
let pythagorean = function(a, b) {
    return ((a ** 2) + (b ** 2)) ** (1/2);
}
console.log(pythagorean(6, 8))

//стрелочная
let pythagorean = (a, b) => ((a ** 2) + (b ** 2)) ** (1/2);
console.log(pythagorean(6, 8))

//3 дискриминант
//Declaration
function des(a, b, c) {
    return (b * b) - (4 * a * c)
}
console.log(des(2, 15, 2))

//Expression
let des = function(a, b, c) {
    return (b * b) - (4 * a * c)
}
console.log(des(2, 15, 2))

//стрелочная
let des = (a,b,c) => (b * b) - (4 * a * c)
console.log(des(2, 15, 2))

//4 четные числа до 100
//Declaration
//for....................................
function evenNum(num) {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 == 0) {
            result.push(i);
        }
    }
    return result
}
console.log(evenNum(100))

//Expression
//for
let evenNum = function(num) {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 == 0) {
            result.push(i);
        }
    }
    return result
}
console.log(evenNum(100))

//стрелочная
//for
let evenNum = (num) => {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 == 0) {
            result.push(i);
        }
    }
    return result
}
console.log(evenNum(100))

//Declaration
//while................................
function evenNum(num) {
    let result = []
    let i = 1
    while ( i <= num) {
        if (i % 2 == 0) {
            result.push(i);
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//Expression
//while
let evenNum = function(num) {
    let result = []
    let i = 1
    while ( i <= num) {
        if (i % 2 == 0) {
            result.push(i);
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//стрелочная
//while
let evenNum = (num) => {
    let result = []
    let i = 1
    while ( i <= num) {
        if (i % 2 == 0) {
            result.push(i);
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//Declaration
//do while.......................................
function evenNum(num) {
    let result = []
    let i = 1
    do {
        if (i % 2 === 0) {
            primes.push(i);
        }
        i++;
} while (i <= num)
 return primes;
}
console.log(evenNum(100))

//Expression
//do while
let evenNum = function(num) {
    let result = []
    let i = 1
    do {
        if (i % 2 === 0) {
            primes.push(i);
        }
        i++;
} while (i <= num)
 return primes;
}
console.log(evenNum(100))

//стрелочная
//do while
let evenNum = (num) => {
    let result = []
    let i = 1
    do {
        if (i % 2 === 0) {
            primes.push(i);
        }
        i++;
} while (i <= num)
    return primes
}
console.log(evenNum(100))

//Declaration
//for of.......................................
function evenNum(num){
    let result = []
    for (let i of num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))


//Expression
//for of
let evenNum = function(num){
    let result = []
    for (let i of num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//стрелочная
//for of
let evenNum = (num) => {
    let result = []
    for (let i of num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//Declaration
//for in.......................................
function evenNum(num){
    let result = []
    for (let i in num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//Expression
//for in
let evenNum = function(num){
    let result = []
    for (let i in num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//стрелочная
//for in
let evenNum = (num) => {
    let result = []
    for (let i in num) {
        if (i % 2 === 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))


// 5- Создать не четные числа до 100
//Declaration
//for.........................................
function evenNum(num) {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 !== 0) {
            result.push(i)
        }
    }
    return result
}
console.log(evenNum(100))

//Expression
//for
let evenNum = function(num) {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 !== 0) {
            result.push(i)
        }
    }
    return result
}
console.log(evenNum(100))

//стрелочная
//for
let evenNum = (num) => {
    let result = [];
    for (let i = 1; i <= num; i++) {
        if (i % 2 !== 0) {
            result.push(i)
        }
    }
    return result
}
console.log(evenNum(100))

//Declaration
//while...............................
function evenNum(num) {
    let result = []
    let i = 1
    while (i <= num) {
        if (i % 2 !== 0) {
            result.push(i)
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//Expression
//while
let evenNum = function(num) {
    let result = []
     let i = 1
    while (i <= num) {
        if (i % 2 !== 0) {
            result.push(i)
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//стрелочная
//while
let evenNum = (num) => {
    let result = []
    let i = 1
    while ( i <= num) {
        if (i % 2 !== 0) {
            result.push(i)
        }
        i++
    }
    return result
}
console.log(evenNum(100))

//Declaration
//do while.......................................
function evenNum(num) {
    let result = []
    let i = 1
    do {
        if (i % 2 !=== 0) {
            primes.push(i)
        }
        i++
} while (i <= num)
 return primes;
}
console.log(evenNum(100))

//Expression
//do while
let evenNum = function(num) {
    let result = []
    let i = 1
    do {
        if (i % 2 !=== 0) {
            primes.push(i)
        }
        i++
} while (i <= num)
 return primes;
}
console.log(evenNum(100))

//стрелочная
//do while
let evenNum = (num) => {
    let result = []
    let i = 1
    do {
        if (i % 2 !=== 0) {
            primes.push(i)
        }
        i++
} while (i <= num)
    return primes
}
console.log(evenNum(100))

//Declaration
//for of.......................................
function evenNum(num){
    let result = []
    for (let i of num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))


//Expression
//for of
let evenNum = function(num){
    let result = []
    for (let i of num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//стрелочная
//for of
let evenNum = (num) => {
    let result = []
    for (let i of num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//Declaration
//for in.......................................
function evenNum(num){
    let result = []
    for (let i in num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//Expression
//for in
let evenNum = function(num){
    let result = []
    for (let i in num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//стрелочная
//for in
let evenNum = (num) => {
    let result = []
    for (let i in num) {
        if (i % 2 !=== 0) {
            result.push(i)
        }
    }
    return result
}
let primes = []
for (let i = 1; i <= 100; i++) {
    primes.push(i)
}
console.log(evenNum(primes))

//6- Создать функцию по нахождению числа в степени
//Declaration
//for................................................
function pow(x, n) {
    let result = x;

    for (let i = 1; i < n; i++) {
        result *= x;
    }
    return result;
}
console.log(pow(2,2))

//Expression
//for
let pow = function(x, n) {
    let result = x;

    for (let i = 1; i < n; i++) {
        result *= x;
    }
    return result;
}
console.log(pow(2,2))


//стрелочная
//for
let pow = (x, n) => {
    let result = x;

    for (let i = 1; i < n; i++) {
        result *= x;
    }
    return result;
}
console.log(pow(2,2))

//Declaration
//while.................................
function pow(x, n) {
    let result = x;
    let i = 1;
    while (i < n) {
        result *= x;
        i++
    }
    return result;
}
console.log(pow(2, 2))

//Expression
//while
let pow = function(x, n) {
    let result = x;
let i = 1;
    while (i < n) {
        result *= x;
        i++
    }
    return result;
}
console.log(pow(2,2))


//стрелочная
//while
let pow = (x, n) => {
    let result = x;
let i = 1;
    while (i < n) {
        result *= x;
        i++
    }
    return result;
}
console.log(pow(2,2))

//Declaration
//do while..................................
function pow(x, n) {
    let result = x;
    let i = 1;
    do {
        result *= x;
        i++
    }while(i < n)
    return result;
}
console.log(pow(2, 2))

//Expression
// do while
let pow = function(x, n) {
    let result = x;
    let i = 1;
     do {
        result *= x;
        i++
    }while(i < n)
    return result;
}
console.log(pow(2,2))


//стрелочная
// do while
let pow = (x, n) => {
    let result = x;
    let i = 1;
    do {
        result *= x;
        i++
    }while(i < n)
    return result;
}
console.log(pow(2,2))


// 7- написать функцию сортировки
//Declaration
//for.......................................
let array = [12, 15, 47 , 54 ,2]
let len = array.length
function sort(arr) {
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
        }
    }
    return arr;
}
console.log(sort(array))

//Expression
//for
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = function(arr) {
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
        }
    }
    return arr;
}
console.log(sort(array))

//стрелочная
//for
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = (arr) => {
    for (let i = 0; i < len - 1; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
        }
    }
    return arr;
}
console.log(sort(array))

//Declaration
//while.......................................
let array = [12, 15, 47 , 54 ,2]
let len = array.length
function sort(arr) {
    let i = 0;
    while ( i < len - 1) {
        let j = 0;
        while ( j < len - 1 - i) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//Expression
//while
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = function(arr) {
    let i = 0;
    while ( i < len - 1) {
        let j = 0;
        while ( j < len - 1 - i) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//стрелочная
//while
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = (arr) => {
   let i = 0;
    while ( i < len - 1) {
        let j = 0;
        while ( j < len - 1 - i) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//Declaration
//do while.......................................
let array = [12, 15, 47 , 54 ,2]
let len = array.length
function sort(arr) {
    let i = 0;
    do {
        let j = 0;
        do {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++;
        }
        while (j < len - i - 1)
        i++;
    }
    while (i < len - 1)
    return arr;

}
console.log(sort(array))

//Expression
//do while
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = function(arr) {
    let i = 0;
     do {
        let j = 0;
        do {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++;
        }
        while (j < len - i - 1)
        i++;
    }
    while (i < len - 1)
    return arr;
}
console.log(sort(array))

//стрелочная
//do while
let array = [12, 15, 47 , 54 ,2]
let len = array.length
let sort = (arr) => {
   let i = 0;
    do {
        let j = 0;
        do {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++;
        }
        while (j < len - i - 1)
        i++;
    }
    while (i < len - 1)
    return arr;
}
console.log(sort(array))

//Declaration
//for of.......................................
let array = [12, 15, 47 , 54 ,2]
function sort(arr) {
    let i = 0
    for (let value of arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//Expression
//for of
let array = [12, 15, 47 , 54 ,2]
let sort = function(arr) {
     let i = 0
    for (let value of arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//стрелочная
//for of
let array = [12, 15, 47 , 54 ,2]
let sort = (arr) => {
    let i = 0
    for (let value of arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//Declaration
//for in.......................................
let array = [12, 15, 47 , 54 ,2]
function sort(arr) {
    let i = 0
    for (let value in arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//Expression
//for in
let array = [12, 15, 47 , 54 ,2]
let sort = function(arr) {
     let i = 0
    for (let value in arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

//стрелочная
//for in
let array = [12, 15, 47 , 54 ,2]
let sort = (arr) => {
    let i = 0
    for (let value in arr) {
        let j = 0
        for (let value of arr) {
            if (arr[j+1] < arr[j]) {
                let t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t;
            }
            j++
        }
        i++
    }
    return arr;
}
console.log(sort(array))

// 8- функцию поиска в массиве
//Declaration
//for
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
function search(arr, value) {
    let find = ''
    for (let i = 0; i < len; i++) {
       if (array[i] === value) {
           find = array[i]
       }
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 45))

//Expression
//for
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = function(arr, value) {
    let find = ''
    for (let i = 0; i < len; i++) {
        if (array[i] === value) {
            find = array[i]
        }
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value

}
console.log(search(array, 45))

//стрелочная
//for
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = (arr, value) => {
    let find = ''
    for (let i = 0; i < len; i++) {
        if (array[i] === value) {
            find = array[i]
        }
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value

}
console.log(search(array, 45))

//Declaration
//while.........................................
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
function search(arr, value) {
    let find = ''
    let i = 0
    while (i < len) {
       if (array[i] === value) {
           find = array[i]
       }
       i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 45))

//Expression
//while
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = function(arr, value) {
    let find = ''
    let i = 0
     while (i < len) {
       if (array[i] === value) {
           find = array[i]
       }
       i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 45))

//стрелочная
//while
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = (arr, value) => {
    let find = ''
    let i = 0
     while (i < len) {
       if (array[i] === value) {
           find = array[i]
       }
       i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 45))

//Declaration
//do while.........................................
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
function search(arr, value) {
    let find = ''
    let i = 0
    do {
        if (array[i] === value) {
            find = array[i]
        }
        i++
    }while (i < len)
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value

}
console.log(search(array, 78))

//Expression
//do while
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = function(arr, value) {
    let find = ''
    let i = 0
    do {
        if (array[i] === value) {
            find = array[i]
        }
        i++
    }while (i < len)
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 45))

//стрелочная
//do while
let array = [12, 15, 45, 65, 49, 78]
let len = array.length
let search = (arr, value) => {
    let find = ''
    let i = 0
    do {
        if (array[i] === value) {
            find = array[i]
        }
        i++
    }while (i < len)
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 15))

//Declaration
//for of
let array = [12, 15, 45, 65, 49, 78]
function search(arr, value) {
    let find = ''
    let i = 0;
    for (let value of array) {
       if (array[i] === value) {
           find = array[i]
       }
       i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 15))

//Expression
//for of
let array = [12, 15, 45, 65, 49, 78]
let search = function(arr, value) {
    let find = ''
   let i = 0;
    for (let value of array) {
        if (array[i] === value) {
            find = array[i]
        }
         i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 15))

//стрелочная
//for of
let array = [12, 15, 45, 65, 49, 78]
let search = (arr, value) => {
    let find = ''
   let i = 0;
    for (let value of array) {
        if (array[i] === value) {
            find = array[i]
        }
         i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return'число не найдено ' + value
}
console.log(search(array, 15))

//Declaration
//for in
let array = [12, 15, 45, 65, 49, 78]
function search(arr, value) {
    let find = ''
    let i = 0;
    for (let value of array) {
       if (array[i] === value) {
           find = array[i]
       }
       i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 15))

//Expression
//for in
let array = [12, 15, 45, 65, 49, 78]
let search = function(arr, value) {
    let find = ''
   let i = 0;
    for (let value of array) {
        if (array[i] === value) {
            find = array[i]
        }
         i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 15))

//стрелочная
//for in
let array = [12, 15, 45, 65, 49, 78]
let search = (arr, value) => {
    let find = ''
    let i = 0;
    for (let value of array) {
        if (array[i] === value) {
            find = array[i]
        }
        i++
    }
    if (find === value) {
        return 'я нашла число ' + value
    }
    return 'число не найдено ' + value
}
console.log(search(array, 44))
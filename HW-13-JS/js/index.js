//1
//for
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrRevers = []
let arrLength = arr.length
for (let i = 0; i < arrLength; i++) {
    arrRevers[i] = arr[(arrLength -1) - i]
}
console.log(arrRevers)

//while
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrRevers = []
let arrLength = arr.length
let i = 0
while (i < arrLength) {
    arrRevers[i] = arr[(arrLength -1) - i]
    i++
}
console.log(arrRevers)

//do while
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrRevers = []
let arrLength = arr.length
let i = 0
do  {
    arrRevers[i] = arr[(arrLength -1) - i]
    i++
} while (i < arrLength)
console.log(arrRevers)

//for of
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrRevers = []
let arrLength = arr.length
let i = 0
for ( let value of arr) {
    arrRevers[i] = arr[(arrLength -1) - i]
    console.log(value)
    i++
}
console.log(arrRevers)

//for in
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrRevers = []
let arrLength = arr.length
let i = 0
for ( let value in arr) {
    arrRevers[i] = arr[(arrLength -1) - i]
    console.log(value)
    i++
}
console.log(arrRevers)

//2
//for
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arrayRevers = []
let arrLength = arr.length
for (i = 0; i < arrLength; i++) {
    arrayRevers[i] = array[(arrLength -1) -i]
}
console.log(arrayRevers)

//while
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arrayRevers = []
let arrLength = arr.length
let i = 0
while (i < arrLength) {
    arrayRevers[i] = array[(arrLength -1) - i]
    i++
}
console.log(arrayRevers)

//for of
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arrayRevers = []
let arrLength = arr.length
let i = 0
for (let value of array){
    arrayRevers[i] = array[(arrLength -1) - i]
    console.log(value)
    i++
}
console.log(arrayRevers)

//for in
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arrayRevers = []
let arrLength = arr.length
let i = 0
for (let value in array){
    arrayRevers[i] = array[(arrLength -1) - i]
    console.log(value)
    i++
}
console.log(arrayRevers)


//3
//for
let str = 'Hi I am ALex'
let arrayRevers = '';
let strLength = str.length
for (let i = 0; i < arrLength; i++) {
    arrayRevers += str[(strLength -1) -i]
}
console.log(arrayRevers)

//while
let str = 'Hi I am ALex'
let arrayRevers = '';
let strLength = str.length
let i = 0
while (i < arrLength) {
    arrayRevers += str[(strLength -1) -i]
    i++
}
console.log(arrayRevers)

//for of
let str = 'Hi I am ALex'
let arrayRevers = '';
let strLength = str.length
let i =0
for (let value of str) {
    arrayRevers += str[(strLength -1) - i]
    i++
}
console.log(arrayRevers)

//for in
let str = 'Hi I am ALex'
let arrayRevers = '';
let strLength = str.length
let i =0
for (let value in str) {
    arrayRevers += str[(strLength -1) - i]
    i++
}
console.log(arrayRevers)

//4
//for
let str = 'Hi I am ALex'
let strLower = ''
let len = str.length
for (let i = 0; i < len; i++) {
    strLower += str[i].toLowerCase()
}
console.log(strLower)

//while
let str = 'Hi I am ALex'
let strLower = ''
let i = 0
let len = str.length
while (i < len) {
    strLower += str[i].toLowerCase()
    i++
}
console.log(strLower)

//for of
let str = 'Hi I am ALex'
let strLower = ''
let i = 0
for (let value of str) {
    strLower += str[i].toLowerCase()
    i++
}
console.log(strLower)

//for in
let str = 'Hi I am ALex'
let strLower = ''
let i = 0
for (let value in str) {
    strLower += str[i].toLowerCase()
    i++
}
console.log(strLower)

//5
//for
let str = 'Hi I am ALex'
let strUpper = ''
let len = str.length
for (let i = 0; i < len; i++) {
    strUpper += str[i].toUpperCase()
}
console.log(strUpper)

//while
let str = 'Hi I am ALex'
let strUpper = ''
let i = 0
let len = str.length
while (i < len) {
    strUpper += str[i].toUpperCase()
    i++
}
console.log(strUpper)

//for of
let str = 'Hi I am ALex'
let strUpper = ''
let i = 0
for (let value of str) {
    strUpper += str[i].toLowerCase()
    i++
}
console.log(strUpper)

//for in
let str = 'Hi I am ALex'
let strUpper = ''
let i = 0
for (let value in str) {
    strUpper += str[i].toLowerCase()
    i++
}
console.log(strUpper)

//7
//for
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayUpper = []
let len = array.length
for (let i = 0; i < len; i++) {
    arrayUpper[i] = array[i].toLowerCase()
}
console.log(arrayUpper)

//while
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayUpper = []
let i = 0
let len = array.length
while (i < len) {
    arrayUpper[i] = array[i].toLowerCase()
    i++
}
console.log(arrayUpper)

//for of
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayUpper = []
let i = 0
for (let value of array) {
    arrayUpper[i] = array[i].toLowerCase()
    i++
}
console.log(arrayUpper)

//for in
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayUpper = []
let i = 0
for (let value in array) {
    arrayUpper[i] = array[i].toLowerCase()
    i++
}
console.log(arrayUpper)

//8
//for
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayLower = []
let len = array.length
for (let i = 0; i < len; i++) {
    arrayLower[i] = array[i].toUpperCase()
}
console.log(arrayLower)

//while
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayLower = []
let i = 0
let len = array.length
while (i < len) {
    arrayLower[i] = array[i].toUpperCase()
    i++
}
console.log(arrayLower)

//for of
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayLower = []
let i = 0
for (let value of array) {
    arrayLower[i] = array[i].toUpperCase()
    i++
}
console.log(arrayLower)

//for in
let array = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let arrayLower = []
let i = 0
for (let value in array) {
    arrayLower[i] = array[i].toUpperCase()
    i++
}
console.log(arrayLower)

//9
//for
let num = 1234678
let numRevers = ''
let  stringLength = num.toString().length
for (let i = 0; i < stringLength; i++) {
    numRevers += num.toString()[(stringLength -1) - i]
}
console.log(parseInt(numRevers))

//while
let num = 1234678
let numRevers = ''
let i = 0
let  stringLength = num.toString().length
while (i < stringLength) {
    numRevers += num.toString()[(stringLength -1) - i]
    i++
}
console.log(parseInt(numRevers))

//for of
let num = 1234678
let numRevers = ''
let i = 0
let  stringLength = num.toString().length
for (let value of num.toString()) {
    numRevers += num.toString()[(stringLength -1) - i]
    i++
}
console.log(parseInt(numRevers))

//for in
let num = 1234678
let numRevers = ''
let i = 0
let  stringLength = num.toString().length
for (let value in num.toString()) {
    numRevers += num.toString()[(stringLength -1) - i]
    i++
}
console.log(parseInt(numRevers))

//10
//for
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let tmpFolder = []
let j = 0
let i = 1
let len = array.length
for (i = 1; i < len; i++) {
    tmpFolder = array[i];
    for (j = i - 1; j >= 0 && (array[j] < tmpFolder); j--) {
        array[j + 1] = array[j];
    }
    array[j + 1] = tmpFolder;
}
console.log(array)

//while
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let tmpFolder = []
let j = 0
let i = 1
let len = array.length
while (i < len) {
    tmpFolder = array[i];
    let j = i - 1
    while (j >= 0 && (array[j] < tmpFolder)) {
        array[j + 1] = array[j];
        j--
    }
    array[j + 1] = tmpFolder;
    i++

}
console.log(array)

//do while
let array = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let tmpFolder = []
let arrayLength = array.length
let j = 0
let i = 1
do {
    tmpFolder = array[i];
    j = i - 1;
    while (j >= 0 && (array[j] < tmpFolder)) {
        array[j + 1] = array[j];
        j--
    }
    array[j + 1] = tmpFolder;
    i++
}while (i < arrayLength)

console.log(array)

//1
let a = 3
alert(a)

//2
let a = 10
let b = 2
alert(a + b)
alert(a - b)
alert(a / b)
alert(a * b)

//3
let c = 15
let d = 2
let result = c + d
alert(result)

//4
let a = 1
let b = 2
let c = 5
let result = a + b + c
alert(result)

//5
let a = 17
let b = 10
let c = a - b
let d = 7
let result = c + d
alert(result)

//6
let text = 'Привет, Мир!'
alert(text)

//7
let text1 = 'Привет'
let text2 = 'Мир!'
alert(`${text1} ${text2}`)

//8
let secondsInHour = 60 * 60
let secondsInDay = secondsInHour * 24
let secondsInWeek = secondsInDay * 7
let secondsInMonth = secondsInWeek * 30
console.log(secondsInHour)
console.log(secondsInDay)
console.log(secondsInWeek)
console.log(secondsInMonth)

//9
let num = 1;
num += 12;
num -= 14;
num *= 5;
num /= 7;
++num;
--num;
console.log(num)

//10
let hour = 15
let minute = 15
let second = 30
alert(`${hour}:${minute}:${second}`)

//11
let text = 'Я';
text += 'хочу' ;
text +='знать' ;
text +='JS!' ;
console.log(text)

//12
let foo = 'bar'
let bar = 10
alert(eval(foo))

//13
let a = 5
let b = 2
console.log(a + b)
console.log(a * b)

//14
let a = 5
let b = 3
let sum = 5 * 5 + 3 * 3
alert(sum)

//15
let a = 5
let b = 2
let c = 3
let sum = a + b + c
let arithMean = (sum / 3)
alert(arithMean)

//16
let x = 2
let y = 3
let z = 4
alert((x+1) - 2*(z-2*x+y));

//17
let num1 = 20 % 3
let num2 = 20 % 5
alert(num1)
alert(num2)

let num = 1000
let percent1 = 30
let result1 =  num + ( num * percent1 / 100 );
let percent2 = 120
let result2 =  num + ( num * percent2 / 100 );
alert(result1)
alert(result2)

//18
let numFirst = 20
let percentFirst = 48
let numSecond =25
let percentSecond = 84
let sumFirst = numFirst + (numFirst * percentFirst / 100)
let sumSecond = numSecond + (numSecond * percentSecond / 100)
alert(sumFirst)
alert(sumSecond)

let number = '365'
let sum = parseInt(num[0]) + parseInt(num[1]) + parseInt(num[2]);
alert(sum)

//19
let num = '258'
let sum = parseInt(num[0] + 0 + num[2]);
let sumRevers = parseInt(num[2] +num[1] + num[0]);
console.log(sum)
console.log(sumRevers)

//20
let a = 1;
let b = 3;
let result1 = a++ + b;
let result2 = a + ++b;
let result3 = ++a + b++
alert(result1)
alert(result2)
alert(result3)

//21
let ocenka = 5;
console.log(3 > 2 ? 'сдал экзамен' : 'не сдал экзамен')
alert('тебе есть 18 лет'? 'хорошо' : 'плохо')
alert('есть ли у тебя загран паспорт'? 'хорошо' : 'плохо')
alert('есть ли тебе 16'? 'хорошо' : 'плохо')

//22
let num = prompt ('число');
alert(num % 2 == 0 ? 'Число чётное': 'Число нечётное');

//23 part second
let a = 42;
let b = 55;
console.log(a > b ? a : b);

//24
let a = prompt ('Введите число от 5 до 15');
let max = 15, min = 5;
let rand = Math.floor(Math.random() * (max - min + 1) + min);
console.log(rand);
console.log(Math.max(5,15));
console.log(Math.min(5,15));

//25
let firstName = 'Алина';
let lastName = 'Федыняк';
let patronymic = 'Виталиевна';
let firstName1 = String(firstName);
let patronymic1 = String(patronymic);
console.log(lastName, firstName[0]+'.'+ patronymic[0] + '.');

//26
let a=12;
let b=14;
let c=10;
let d=-12;
console.log(Math.max(a,b,c,d));
console.log(Math.min(a,b,c,d));

//27
let a = 55;
let b = 100;
let c =- 15;
let d = 8;
let e =- 4;
let f = 10;
console.log(Math.max(a,b,c,d,e,f));
console.log(Math.min(a,b,c,d,e,f));

//28
let AB = 8;
let BC = 8;
let AC = 4;
console.log(AB===BC && BC===AC? 'Равносторонний' : 'Равнобедренный');
if(AB===BC && BC===AC){
    console.log('Равносторонний');
}
else{
    console.log('Равнобедренный');
};

//29
let AB = 15;
let BC = 10;
let CD = 15;
let AD = 10;
console.log(AB===BC && CD===AD? 'Квадрат' : 'Прямоугольник');
if(AB===BC && CD===AD){
    console.log('Квадрат');
}
else{
    console.log('Прямоугольник');
};

//30
let str = '12345678999'
let len = str.length
let search = prompt('введите число для поиска')
let result = 0

for (let i = 0; i < str.length; i++) {
    if(str[i] === search) {
        result++
    }
}
alert(result)

//31
switch (month > 0) {
    case month == 12 || month <=2:
        console.log('Зима');
        break;
    case month >= 3 && month <=5:
        console.log('Весна');
        break;
    case month >= 6 && month <=8:
        console.log('Лето');
        break;
    case (month >=9 && month <=11):
        console.log('Осень');
        break;
};

//32
let str = 'abcde'
if (str[0] === 'a') {
    console.log('да')
}else {
    console.log('нет')
}

//33
let str = '12345'
if (str[0] == '1' && str[1] == '2' && str[2] == '3') {
    console.log('да')
}else {
    console.log('нет')
}

//34
let test = true
console.log(test === true? 'верно': 'неверно')

if (test === true) {
    console.log('верно')
}else {
    console.log('неверно')
}

//35
let lang = 'ru'
let arr = [['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'], ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']];
if (lang === 'ru') {
    console.log(arr[0])
}else {
    console.log(arr[1])
}

let lang = 'ru';
console.log(lang ==='ru'? ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'] : ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']);
if (lang === 'ru') {
    console.log('пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс')
}
else {
    console.log('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun')
};

//36
let clock = prompt('введите число для проверки')
console.log(clock  > 15 && clock <= 30? 'вторая четверть' : 'другая четверть')

 if (clock >= 0 && clock <= 15) {
     console.log('первая четверть')
 }else if (clock  > 15 && clock <= 30) {
     console.log('вторая четверть')
 }else if (clock  > 30 && clock <= 45) {
     console.log('третья четверть')
 }else if (clock  > 30 && clock <= 59) {
     console.log('четвертая четверть')
 }


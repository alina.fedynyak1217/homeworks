//1
let firstName = 'Alina'
let lastName = 'Fedyniak'
let age = 28

alert(firstName)
alert(lastName)
alert(age)
console.log(firstName)
console.log(lastName)
console.log(age)

//2
const FIRST_NAME = 'Alina'
const LAST_NAME = 'Fedyniak'
const AGE = 28

alert(FIRST_NAME)
alert(LAST_NAME)
alert(AGE)
console.log(FIRST_NAME, LAST_NAME, AGE)

//3
let firstName = prompt('Как тебя зовут?')
let lastName = prompt('Какая фамилия?')
let age = prompt('Сколько тебе лет?')

alert(firstName + ' ' + lastName + ' ' + age)
console.log(`${firstName} ${lastName} ${age}`)

//4
let result  = confirm('Есть ли вам 18 лет?')
alert(result)
console.log(result)

//5
let date = '2021.04.20'

alert(date)
console.log(date)

//6
let date = prompt('Какая дата сегодня?', '20.04.2021')

alert(date)
console.log(date)

//7
let apple = prompt('Сколько яблок у тебя?')
let orange = prompt('Сколько апельсин у тебя?')

let result = Number(apple) + Number(orange)

alert(result)
console.log(result)

const BACK_BODY = 'grey'
document.body.style.background = BACK_BODY

let colorsArr = ['#D7BDE2', '#ABEBC6', '#A3E4D7', '#D6DBDF', '#F2D7D5']
document.body.style.background = colorsArr[1]

let colorObj = {LightSalmon: '#FFA07A', Yellow: '#FFFF00', DarkSalmon: '#E9967A', Aqua: '#00FFFF', Teal: '#008080'}
document.body.style.background = colorObj.LightSalmon


//8
let person =  {
    firstName: 'Alina',
    lastName: 'Fedyniak',
    nickname: 'alina',
    photo: 'https://bipbap.ru/wp-content/uploads/2018/01/depositphotos_6014361-stock-illustration-funny-rabbit-with-carrot-640x640.jpg',
    age: 28,
    male: 'woman',
    amail: 'ddhghdh.djdj@mail.com'
}

let res = `${person.firstName}</br>
           ${person.lastName}</br>
           ${person.nickname}</br>
           ${person.photo}</br>
           ${person.age}</br>
           ${person.male}</br>
           ${person.amail}</br>`

document.getElementById('person').innerHTML = res
alert(person.nickname)
console.log(person)

//9
let car = {
    model: 'BMW i8',
    color: 'red',
    brand: 'German',
    year: 2015,
    bodyType: 'compartment',
    numberOfPlaces: 2 + 2,
    height: 4689,
    width: 1293,
    engineVolume: 1.5,
    power: '231 h.p.',
    transmission: 'machine',
    engine: 'electric',
    rechargeable: '11.6 kWh',
    racing: '4,4 s',
    doors: 2,
    numberOfValves: 12,
    driveUnit: 'full'
}
let res = `${car.model}</br>
           ${car.color}</br>
           ${car.brand}</br>
           ${car.year}</br>
           ${car.bodyType}</br>
           ${car.numberOfPlaces}</br>
           ${car.height}</br>
           ${car.width}</br>
           ${car.engineVolume}</br>
           ${car.power}</br>
           ${car.transmission}</br> 
           ${car.engine}</br>
           ${car.rechargeable}</br>
           ${car.racing}</br>
           ${car.doors}</br>
           ${car.numberOfValves}</br>
           ${car.driveUnit}</br>`
document.getElementById('car').innerHTML = res
alert(car.model)
console.log(car)


//10
let house = {
    type: 'house',
    address: 'Kharkov',
    PartOfHouse: 1,
    totalArea: 93,
    landArea: 4,
    kitchen: 'big',
    bedroom: 2,
    bathroom: 'big',
    garage: 'yes',
    cellar: 'yes',
    gas: 'yes',
    water: 'pump',
    numberOfStoreys: 1,
    sewerage: 'yes',
    distanceToCity: 'in city',
    price: '70 000 $'
}
let res = `${house.type}</br>
           ${house.address}</br>
           ${house.PartOfHouse}</br>
           ${house.totalArea}</br>
           ${house.landArea}</br>
           ${house.kitchen}</br>
           ${house.height}</br>
           ${house.width}</br>
           ${house.bedroom}</br>
           ${house.bathroom}</br>
           ${house.garage}</br> 
           ${house.cellar}</br>
           ${house.gas}</br>
           ${house.water}</br>
           ${house.numberOfStoreys}</br>
           ${house.sewerage}</br>
           ${house.distanceToCity}</br>
           ${house.price}</br>`
document.getElementById('house').innerHTML = res
alert(house.distanceToCity)
console.log(house)

